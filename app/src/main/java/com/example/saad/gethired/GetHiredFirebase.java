package com.example.saad.gethired;


import com.example.saad.gethired.Models.UserModel;
import com.firebase.client.Firebase;

/**
 * Created by Rifhan on 5/4/2016.
 */
public class GetHiredFirebase extends android.app.Application {
    public static final  String DATABASE_URL = "https://get-hired.firebaseio.com";
    private static Firebase user;
    private static Firebase jobs;
    private static Firebase requests;
    @Override
    public void onCreate( ) {
        super.onCreate();
        Firebase.setAndroidContext(this);

        UserModel.email = "rifhan.akram1@gmail.com";

    }

    public static Firebase getFirebaseRef(String name){
        if(name.equalsIgnoreCase("users")){
            if (user != null){
                return user;
            }else {
                return user = new Firebase(DATABASE_URL+"/Users");
            }
        }else if(name.equalsIgnoreCase("jobs")){
            if(jobs != null){
                return jobs;
            }else{
                return jobs = new Firebase(DATABASE_URL+"/Jobs");
            }
        }else if(name.equalsIgnoreCase("requests")){
            if(requests != null)
                return requests;
            else
                return  requests = new Firebase(DATABASE_URL+"/Requests");
        }else{
            return null;
        }
    }
}
