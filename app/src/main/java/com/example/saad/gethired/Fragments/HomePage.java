package com.example.saad.gethired.Fragments;

import android.database.Cursor;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.saad.gethired.DBHandler;
import com.example.saad.gethired.GetHiredFirebase;
import com.example.saad.gethired.Home;
import com.example.saad.gethired.Home_View_Adapter;
import com.example.saad.gethired.IndividualPost;
import com.example.saad.gethired.Models.JobModel;
import com.example.saad.gethired.Models.RequestModel;
import com.example.saad.gethired.R;
import com.example.saad.gethired.SubmitPost;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 *
 * to handle interaction events.
 * Use the {@link HomePage#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomePage extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View view ;
    private TabLayout tabLayout;
    private CardView card;
    private FloatingActionButton addPosts;
    List<JobModel> jobs;

    public HomePage() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomePage.
     */
    // TODO: Rename and change types and number of parameters
    public static HomePage newInstance(String param1, String param2) {
        HomePage fragment = new HomePage();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        List<String[]> data = new Home().fillData();
        List<JobModel> data = fillData();
        /* setting up recycler view and card view*/
        view = inflater.inflate(R.layout.fragment_home_page, container, false);
        RecyclerView homeView = (RecyclerView) view.findViewById(R.id.home_view);
        Home_View_Adapter adapter = new Home_View_Adapter(data,new Home());
        homeView.setAdapter(adapter);
        homeView.setLayoutManager(new LinearLayoutManager(new Home()));
        //card = (CardView) view.findViewById(R.id.card);


        return view;
    }

    private List<JobModel> fillData(){

        jobs = new ArrayList<>();
        Firebase jobRef = new Firebase(GetHiredFirebase.DATABASE_URL+"/Jobs");

        jobRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                System.out.println("There are " + snapshot.getChildrenCount() + " blog posts");
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    JobModel post = postSnapshot.getValue(JobModel.class);
                    jobs.add(new JobModel(post.getJob_id(), post.getTitle(), post.getDescription()
                            , post.getLocation(), post.getTimestamp(), post.getAmount()
                            , post.getUser_id()));
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println("The read failed: " + firebaseError.getMessage());
            }
        });
        return jobs;

    }





    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

}
