package com.example.saad.gethired;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.support.v7.widget.CardView;

/**
 * Created by Rifhan on 3/16/2016.
 */
public class View_Holder extends RecyclerView.ViewHolder{
    CardView card;
    TextView title;
    TextView description;
    TextView location;
    TextView amount;
    TextView timestamp;
    TextView postId;

    public View_Holder(View itemView) {
        super(itemView);
        card = (CardView) itemView.findViewById(R.id.card);
        title = (TextView) itemView.findViewById(R.id.title);
        description = (TextView) itemView.findViewById(R.id.description);
        location = (TextView) itemView.findViewById(R.id.location);
        amount = (TextView) itemView.findViewById(R.id.amount);
        timestamp = (TextView) itemView.findViewById(R.id.timestamp);
        postId = (TextView) itemView.findViewById(R.id.postId);
        card.setOnClickListener(new CardView.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent signUpActivity = new Intent(LogIn.this, SignUp.class);
                Intent individualPostActivity = new Intent(v.getContext(), IndividualPost.class);
                individualPostActivity.putExtra("postId", postId.getText().toString());
                individualPostActivity.putExtra("title", title.getText().toString());
                individualPostActivity.putExtra("desc", description.getText().toString());
                individualPostActivity.putExtra("loc", location.getText().toString());
                individualPostActivity.putExtra("amt", amount.getText().toString());
                individualPostActivity.putExtra("timestamp", timestamp.getText().toString());
                v.getContext().startActivity(individualPostActivity);
            }
        });
    }
}
