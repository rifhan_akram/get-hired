package com.example.saad.gethired.Models;

/**
 * Created by saad on 5/5/16.
 */
public class UserModel {
    public String user_id;
    public String username;
    public String password;
    public String name;
    public static String email;
    public String contact;
    public String address;
    public String remember_token;
    public String points;
    public String interests;
    public String expertise;
    public String type;

    public UserModel( String username, String password, String name,
                     String contact, String address, String remember_token, String points,
                     String interests, String expertise, String type) {

        this.username = username;
        this.password = password;
        this.name = name;
        this.contact = contact;
        this.address = address;
        this.remember_token = remember_token;
        this.points = points;
        this.interests = interests;
        this.expertise = expertise;
        this.type = type;
    }

    public UserModel() {
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRemember_token() {
        return remember_token;
    }

    public void setRemember_token(String remember_token) {
        this.remember_token = remember_token;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(String interests) {
        this.interests = interests;
    }

    public String getExpertise() {
        return expertise;
    }

    public void setExpertise(String expertise) {
        this.expertise = expertise;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
