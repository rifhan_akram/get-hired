package com.example.saad.gethired.Models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rifhan on 3/16/2016.
 */
public class JobModel {
    public String job_id;
    public String title;
    public String description;
    public String location;
    public String timestamp;
    public String amount;
    public String user_id;
    public static List<JobModel> data;
    public JobModel(){

    }

    public JobModel(String job_id, String title, String description, String location,
                    String timestamp, String amount, String user_id) {
        this.job_id = job_id;
        this.title = title;
        this.description = description;
        this.location = location;
        this.timestamp = timestamp;
        this.amount = amount;
        this.user_id = user_id;
    }

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public static List<JobModel> getData(){
        if(data != null){
            return data;
        }else{
            return data = new ArrayList<JobModel>();
        }
    }


}