package com.example.saad.gethired;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.saad.gethired.Models.UserModel;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.CallbackManager;
import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import android.database.Cursor;


public class LogIn extends AppCompatActivity {

    DBHandler dbHandler;

    EditText username;
    EditText password;

    private LoginButton loginButton;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_log_in);

        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton)findViewById(R.id.login_button);

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("user", loginResult.getAccessToken().getUserId());
                Log.d("token", loginResult.getAccessToken().getToken());
                Intent homepage = new Intent(LogIn.this,Home.class);
                startActivity(homepage);
            }

            @Override
            public void onCancel() {
                Log.d("cancel", "Login attempt canceled.");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("fail", "Login attempt failed.");
            }
        });

        dbHandler = new DBHandler(this, null, null);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);

        Button signin = (Button) findViewById(R.id.signin);
        final Button signup = (Button) findViewById(R.id.signup);


        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateUser();

            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signUpActivity = new Intent(LogIn.this, SignUp.class);
                startActivity(signUpActivity);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void validateUser() {
        final String user = username.getText().toString();
        String pass = password.getText().toString();

        if(user.isEmpty() && pass.isEmpty()) {
            Toast.makeText(LogIn.this, "Username and Password fields are Wmpty!", Toast.LENGTH_SHORT).show();
        }
        else if(user.isEmpty()) {
            Toast.makeText(LogIn.this, "Username field is Empty!", Toast.LENGTH_SHORT).show();
        }
        else if(pass.isEmpty()) {
            Toast.makeText(LogIn.this, "Password field is Empty!", Toast.LENGTH_SHORT).show();
        }
        else {
            Firebase ref = new Firebase(GetHiredFirebase.DATABASE_URL);
            ref.authWithPassword(user, pass, new Firebase.AuthResultHandler() {
                @Override
                public void onAuthenticated(AuthData authData) {
                    UserModel.email = user;
                    Intent homepage = new Intent(LogIn.this,Home.class);
                    startActivity(homepage);
                }
                @Override
                public void onAuthenticationError(FirebaseError firebaseError) {
                    // there was an error
                }
            });
        }
    }
}
