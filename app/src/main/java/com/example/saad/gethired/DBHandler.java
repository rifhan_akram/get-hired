package com.example.saad.gethired;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by saad on 3/12/16.
 */
public class DBHandler extends SQLiteOpenHelper {

    //private static  final  int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "get_hired.db";

    public DBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory) {
        super(context, DATABASE_NAME, factory, 16);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String queryUser = "CREATE TABLE user(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, password TEXT, name TEXT, email TEXT, " +
                "contact INTEGER, address TEXT, remember_token DATE, points INTEGER, " +
                "interests TEXT, expertise TEXT, type INTEGER)";

        String queryJobs = "CREATE TABLE jobs(id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, description TEXT, " +
                "location TEXT, timestamp DATETIME, amount DOUBLE, feedback TEXT, rating INTEGER, user_id INTEGER)";

        String queryRequest = "CREATE TABLE requests(id INTEGER PRIMARY KEY AUTOINCREMENT, message TEXT, " +
                "status INTEGER, user_id INTEGER, job_id INTEGER)";

        db.execSQL(queryUser);
        db.execSQL(queryJobs);
        db.execSQL(queryRequest);

        String insert = "insert into jobs values(1,'dog walk', 'lorem ipsum lorem ipsum lorem ipsum', 'colombo', '12:02:10', 1000, 'good', 2, 1)";
        db.execSQL(insert);
        String insertq = "insert into jobs values(2,'cat walk', 'cat walk cat walk cat walk', 'colombo', '12:02:10', 1000, 'good', 2, 1)";
        db.execSQL(insertq);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS user");
        db.execSQL("DROP TABLE IF EXISTS jobs");
        db.execSQL("DROP TABLE IF EXISTS requests");
        onCreate(db);
    }

}
