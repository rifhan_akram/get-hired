package com.example.saad.gethired.Models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saad on 5/5/16.
 */
public class RequestModel {
    public String request_id;
    public String message;
    public String status;
    public String user_mail;
    public String job_id;
    public static List<RequestModel> data;
    public RequestModel(){

    }

    public RequestModel(String request_id, String message, String status, String user_mail, String job_id) {
        this.request_id = request_id;
        this.message = message;
        this.status = status;
        this.user_mail = user_mail;
        this.job_id = job_id;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser_mail() {
        return user_mail;
    }

    public void setUser_id(String user_id) {
        this.user_mail = user_mail;
    }

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    public static List<RequestModel> getData(){
        if(data != null){
            return data;
        }else{
            return data = new ArrayList<RequestModel>();
        }
    }
}
