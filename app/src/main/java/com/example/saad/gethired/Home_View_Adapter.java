package com.example.saad.gethired;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.saad.gethired.Models.JobModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by Rifhan on 3/16/2016.
 */
public class Home_View_Adapter extends RecyclerView.Adapter<View_Holder> {

    List<JobModel> list = Collections.emptyList();
    Context context;

    public Home_View_Adapter(List<JobModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public View_Holder onCreateViewHolder(ViewGroup parent,int viewType){
        //inflate the layout and init the view holder
        View view = LayoutInflater.from(parent.getContext()).inflate( R.layout.card_view,parent,false);
        View_Holder holder = new View_Holder(view);

        return holder;

    }

    @Override
    public void onBindViewHolder(View_Holder holder, int position){
        holder.title.setText(list.get(position).title);
        holder.description.setText(list.get(position).description);
        holder.location.setText(list.get(position).location);
        holder.amount.setText(list.get(position).amount);
        holder.timestamp.setText(list.get(position).user_id);
        holder.postId.setText(list.get(position).job_id);
    }

    @Override
    public int getItemCount(){
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, JobModel data) {
        list.add(position, data);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(JobModel data) {
        int position = list.indexOf(data);
        list.remove(position);
        notifyItemRemoved(position);
    }
}

