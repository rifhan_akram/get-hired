package com.example.saad.gethired;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.saad.gethired.Models.JobModel;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;

public class SubmitPost extends AppCompatActivity {
    private EditText title;
    private EditText description;
    private EditText amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_post);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }
    public void submitPost (View view){
        title = (EditText) findViewById(R.id.Post_title);
        description = (EditText) findViewById(R.id.Post_amount);
        amount = (EditText) findViewById(R.id.Post_description);

        EditText[] arr = {title,amount,description};
        if(!validate(arr)){
            Toast.makeText(SubmitPost.this, "Fields Cannot be Empty!!", Toast.LENGTH_SHORT).show();
        }else{
            //submit to firebase database
            Firebase jobs = GetHiredFirebase.getFirebaseRef("jobs");
            try {
                Query jobId = jobs.orderByChild("post_id").limitToLast(1);
                jobId.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        JobModel jobsData = dataSnapshot.getValue(JobModel.class);
                        System.out.println(jobsData.getJob_id());
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });
            } catch(NullPointerException exception){
                System.out.println("Exception Log"+exception);
            }

        }
    }

    public boolean validate(EditText[] arr){
//        int i = 0;
//        int count = 0;
//        int Err[] = new int[arr.length];
//        for (EditText item : arr){
//            if (!item.getText().toString().isEmpty()){
//                Err[i] = count;
//                i++;
//            }
//        count++;
//        }
//        if (Err.length != 0){
//            return false;
//        }else{
//            return true;
//        }
        return  true;
    }
}
