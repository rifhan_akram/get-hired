package com.example.saad.gethired;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.util.Log;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.saad.gethired.Models.RequestModel;
import com.example.saad.gethired.Models.UserModel;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class IndividualPost extends AppCompatActivity {

    TextView user;
    TextView title;
    TextView description;
    TextView location;
    TextView amount;
    TextView timeStamp;
    EditText message;
    Button request;

    String comment;
    Firebase requests;
    Query queryRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_individual_post);

        user = (TextView) findViewById(R.id.postOwner);
        title = (TextView) findViewById(R.id.title);
        description = (TextView) findViewById(R.id.description);
        location = (TextView) findViewById(R.id.location);
        amount = (TextView) findViewById(R.id.amount);
        timeStamp = (TextView) findViewById(R.id.timestamp);
        message = (EditText) findViewById(R.id.comment);

        user.getResources().getColorStateList(R.color.linkcolor);

        request = (Button) findViewById(R.id.request);
        ImageButton userImage = (ImageButton) findViewById(R.id.imageButton);

        final String selectedPostId = getIntent().getStringExtra("postId");
        final String title = getIntent().getStringExtra("title");
        final String desc = getIntent().getStringExtra("desc");
        final String loc = getIntent().getStringExtra("loc");
        final String amt = getIntent().getStringExtra("amt");
        final String timestamp = getIntent().getStringExtra("timestamp");
        final String userId = UserModel.email;

        generateData(selectedPostId, title, desc, loc, amt, timestamp);

        user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signUpActivity = new Intent(IndividualPost.this, SignUp.class);
                startActivity(signUpActivity);
            }
        });

        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signUpActivity = new Intent(IndividualPost.this, SignUp.class);
                startActivity(signUpActivity);
            }
        });

        request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postRequest(selectedPostId, userId);
            }
        });
    }

    public void generateData(String selectedPostId, String title, String desc, String loc, String amt, String timestamp) {

        user.setText("ss");
        this.title.setText(title);
        description.setText(desc);
        location.setText(loc);
        timeStamp.setText(timestamp);
        amount.setText("Rs." + amt);

//        DBHandler handler = new DBHandler(getBaseContext(), null, null);
//
//        String queryJobs = "SELECT * FROM jobs WHERE id = ?";
//        Cursor dataJobs = handler.getReadableDatabase().rawQuery(queryJobs, new String[]{selectedPostId});
//
//        dataJobs.moveToFirst();
//        String userId = dataJobs.getString(8);
//
//        String queryUser = "SELECT name FROM user where id = ?";
//        Cursor dataUser = handler.getReadableDatabase().rawQuery(queryUser, new String[] {userId});
//
//        if(dataUser.getCount() != 0) {
//            dataUser.moveToFirst();
//
//            user.setText(dataUser.getString(0));
//            title.setText(dataJobs.getString(1));
//            description.setText(dataJobs.getString(2));
//            location.setText(dataJobs.getString(3));
//            timeStamp.setText(dataJobs.getString(4));
//            amount.setText("Rs." + dataJobs.getString(5));
//        }
//
//        handler.close();
    }

    public void postRequest(final String selectedPostId, final String userId) {
        List<RequestModel> reqData = RequestModel.getData();
        int flag = 0;
        for (RequestModel value:
             reqData) {
            System.out.println(value);
            if(value.getUser_mail() == userId){
                if (value.getJob_id() == selectedPostId){
                    flag = 1;
                    if(value.getStatus() == "0"){
                        Toast.makeText(IndividualPost.this, "Your request is still on pending!", Toast.LENGTH_LONG).show();
                        break;
                    }else{
                        continue;
                    }
                }else{

                }
            }else{
                continue;
//                if (reqData == null){
//                    reqData.add(new RequestModel("1",message.getText().toString(),"0",userId,selectedPostId));
//                    Toast.makeText(IndividualPost.this, "Your request was successful!", Toast.LENGTH_LONG).show();
//                }else{
//                    String id = Integer.toString(Integer.parseInt(reqData.get(reqData.size()-1).request_id)+1);
//                    reqData.add(new RequestModel(id,message.getText().toString(),"0",userId,selectedPostId));
//                    Toast.makeText(IndividualPost.this, "Your request was successful!", Toast.LENGTH_LONG).show();
//                }
            }
        }
        if (flag != 1){
            if (reqData.isEmpty()){
                    reqData.add(new RequestModel("1", message.getText().toString(), "0", userId, selectedPostId));
                    Toast.makeText(IndividualPost.this, "Your request was successful!", Toast.LENGTH_LONG).show();
                    Intent home = new Intent(IndividualPost.this,Home.class);
                    startActivity(home);
            }else{
                    String id = Integer.toString(Integer.parseInt((reqData.get(reqData.size())).request_id)+1);
                    reqData.add(new RequestModel(id,message.getText().toString(),"0",userId,selectedPostId));
                    Toast.makeText(IndividualPost.this, "Your request was successful!", Toast.LENGTH_LONG).show();
                    Intent home = new Intent(IndividualPost.this,Home.class);
                    startActivity(home);
            }
        }

//        Firebase db = GetHiredFirebase.getFirebaseRef("requests");
//        db.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                for (DataSnapshot data: dataSnapshot.getChildren()) {
//                    RequestModel model = data.getValue(RequestModel.class);
//                    if(model.getUser_mail().equals(userId) && model.getJob_id().equals(selectedPostId)) {
//                        if(model.getStatus().equals("0")) {
//                            Toast.makeText(IndividualPost.this, "Your request is still on pending!", Toast.LENGTH_LONG).show();
//                        }
//                        else {
//                            Toast.makeText(IndividualPost.this, "Yosdsdsdsd!", Toast.LENGTH_LONG).show();
//                        }
//                    }
//                }
//            }
//
//            @Override
//            public void onCancelled(FirebaseError firebaseError) {
//
//            }
//        });

//        DBHandler handler = new DBHandler(getBaseContext(), null, null);
//
//        comment = message.getText().toString();
//
//        String checkInsertQuery = "SELECT * FROM requests WHERE user_id = ? AND job_id = ? AND status = 0";
//        Cursor data = handler.getReadableDatabase().rawQuery(checkInsertQuery, new String[]{userId, selectedPostId});
//
//        if(data.getCount() == 0) {
//            String insertRequestQuery = "INSERT INTO requests (message, status, user_id, job_id) VALUES " +
//                    "('" + comment + "', 0, "+ userId +", "+ selectedPostId +")";
//            handler.getWritableDatabase().execSQL(insertRequestQuery);
//
//            AlertDialog.Builder alert  = new AlertDialog.Builder(this);
//            alert.setTitle("Information");
//            alert.setMessage("You request was Successful!");
//            alert.setPositiveButton("Ok", null);
//            alert.create().show();
//        }
//        else {
//            Toast.makeText(IndividualPost.this, "Your request is still on pending!", Toast.LENGTH_LONG).show();
//        }
//
//        handler.close();
    }
}
