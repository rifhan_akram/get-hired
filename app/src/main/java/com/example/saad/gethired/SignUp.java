package com.example.saad.gethired;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import android.content.DialogInterface;
import android.app.AlertDialog;
import android.util.Log;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.Map;

public class SignUp extends AppCompatActivity {

    DBHandler dbHandler;

    EditText name;
    EditText email;
    EditText username;
    EditText password;
    EditText confirmPassword;
    EditText contact;
    EditText address;
    EditText interests;
    EditText expertise;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        DBHandler dbHandler = new DBHandler(this, null, null);

        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        confirmPassword = (EditText) findViewById(R.id.confirmPassword);
        contact = (EditText) findViewById(R.id.contact);
        address = (EditText) findViewById(R.id.address);
        interests = (EditText) findViewById(R.id.interests);
        expertise = (EditText) findViewById(R.id.expertise);

        Button signup = (Button) findViewById(R.id.signup);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate(new EditText[]{name, email, username, password, confirmPassword, contact, address});
            }
        });
    }

    public void validate(EditText[] fields) {
        for(int i=0; i<fields.length; i++){
            EditText currentField = fields[i];
            if(currentField.getText().toString().length() <= 0){
                Toast.makeText(SignUp.this, "Starred Fields cannot be Empty!", Toast.LENGTH_SHORT).show();
                return;
            }
        }



        String sName = name.getText().toString();
        String sEmail = email.getText().toString();
        String sUsername = username.getText().toString();
        String sPassword = password.getText().toString();
        String sConfirmPassword = confirmPassword.getText().toString();
        String sContact = contact.getText().toString();
        String sAddress = address.getText().toString();
        String sInterests = interests.getText().toString();
        String sExpertise = expertise.getText().toString();

        if(sPassword.equals(sConfirmPassword)) {
            Firebase ref = new Firebase(GetHiredFirebase.DATABASE_URL);
            ref.createUser(sEmail, sPassword, new Firebase.ValueResultHandler<Map<String, Object>>() {
                @Override
                public void onSuccess(Map<String, Object> result) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(new SignUp());
                    alert.setTitle("Information");
                    alert.setMessage("You have been successfully Registered!");
                    alert.setPositiveButton("Ok", null);
//            alert.setPositiveButton("Ok",
//                    new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//                            Intent logInActivity = new Intent(SignUp.this, LogIn.class);
//                            startActivity(logInActivity);
//                        }
//                    });

                    alert.create().show();
                    Intent logInActivity = new Intent(SignUp.this, LogIn.class);
                    startActivity(logInActivity);
                }

                @Override
                public void onError(FirebaseError firebaseError) {
                    // there was an error
                }
            });

            for(int i=0; i<fields.length; i++){
                EditText currentField = fields[i];
                currentField.setText("");
            }

        }
        else {
            password.setText("");
            confirmPassword.setText("");
            Toast.makeText(SignUp.this, "Password do no match!", Toast.LENGTH_SHORT).show();
        }

    }
}
